function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("homepage", "https://shn.world");
define("jwtsecret", 'str0ng hum@n network k3y')
//define("homepage", "http://localhost:3000"); // for dev testing
