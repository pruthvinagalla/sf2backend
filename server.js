
var express = require('express');
var app = express();
var fs = require('fs');
var apicalls = require('./controllers/apicalls');

var key = fs.readFileSync('./shn.client.key');
var cert = fs.readFileSync( './shn.client.crt' );
//var ca = fs.readFileSync( 'encryption/intermediate.crt' );

var options = {
  key: key,
  cert: cert
}


app.use('/apicalls', apicalls);

var https = require('https');
https.createServer(options, app).listen(3001);


//app.listen(3001);
