Prerequisites: Install git tool https://desktop.github.com/                                                                  
Install Docker software on your machine https://www.docker.com/community-edition

steps to run the project:

clone the repository                                              
In Terminal, From project root folder,run below docker commands:                                                                                                                             
a) docker build -t sf2backend .                                                                                                                             
b) docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -p 3002:3001 --rm sf2backend                               
Open your browser to http://localhost:3002/ and you should see the app                                                         
