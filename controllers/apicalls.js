var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var mysql = require('mysql');
const fileUpload = require('express-fileupload');
var router = express.Router();
var app = express();
var nodemailer = require('nodemailer');
const uuidv1 = require('uuid/v1');
var fs = require('fs'); //Filesystem
var constants = require("../static/js/constants");
const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
// Configure MySQL connection.

var mysql_pool  = mysql.createPool({
  connectionLimit : 50,
 // host: 'shn.world',
// host: 'localhost',
 //user: 'root',
 //password: '',
 //database: 'databaseone'

  host: '192.185.129.72',
  user: 'shnwo2bp_Admin',
  password: '$hnCM2018',
  database: 'shnwo2bp_test'
});

router.use(bodyParser.urlencoded({ extended: false }));
router.use(fileUpload());
router.use('/public', express.static(__dirname + '/public'));
router.use(bodyParser.json());
router.use(cors());

//Router for form and file upload implementation for submitting resumes
router.post('/fileUpload', (req, res, next) => {
  let resumeFile = req.files.file;
  // console.log(req.body.fullname);
  // console.log(req.body.email);
  // console.log(req.body.phone);
  // console.log(req.body.address);
  // console.log(req.body.age);
  // console.log(req.body.gender);


  resumeFile.mv(`${__dirname}/public/${req.body.filename}`, function(err) {
    if (err) {
      return res.status(500).send(err);
    }
    var transporter = nodemailer.createTransport({
           host: '192.185.129.72',
           port: 25,
           secure: false,
           auth: {
             user: 'autosend@shn.world',
             pass: 'Restart@123'
           },
           tls:{
             rejectUnauthorized: false
           }
         });

                   var mailOptions = {
           from: 'autosend@shn.world',
           to: 'sailesh@shn.world',
           subject: 'Resume',
           text: 'That was easy!',
           html: 'hola hola',
           attachments: [{   // utf-8 string as an attachment
             // path: '${__dirname}/public/${req.body.filename}'
             path: `${__dirname}/public/${req.body.filename}`
           }]
         };

         transporter.sendMail(mailOptions, function(error, info){
           if (error) {
             console.log(error);
           } else {
             console.log('Email sent: ' + info.response);
           }
         });
    res.json({file: `public/${req.body.filename}`});
  });

})


router.get('/', function(req, res){
   res.send('GET route on things.');
});

//Router for older version of the website (Not implemented anymore)
router.post('/contactus', function(req, res){
  var currentTime = new Date();
  var currentOffset = currentTime.getTimezoneOffset();
  var ISTOffset = 330;   // IST offset UTC +5:30
  var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);
  req.body.phone = "0000000000";
  var values = [req.body.fullname,req.body.email,req.body.phone,req.body.subject,req.body.message,ISTTime];


  mysql_pool.getConnection(function(err, connection) {
    if (err) {

      console.log(' Error getting mysql_pool connection: ' + err);
      throw err;
    }
    connection.query('INSERT INTO SHN_ContactUs (FullName, EmailID, PhoneNumber, Subject, Message, InsertedTime) VALUES (?,?,?,?,?,?)', values, function(err,result) {
      if(err) {
        res.send('unsuccessful');
        console.log(err);
      }
      else {
       res.send('successful');
       console.log(" Message delivered success");
      }
    });
    connection.release();
  });
});

//check if the ref id exists...
router.get('/get_ref', function(req, res){
  var s = '';
  mysql_pool.getConnection(function(err, connection) {
    if (err) {

      console.log(' Error getting mysql_pool connection: ' + err);
      throw err;
    }
    //check if the ref is valid
    var buffer = req.param('ref');
    var ref = Buffer.from(buffer, 'base64');
    console.log("decoded ref "+ ref);
    var select = 'select count(*) as tot from Users where ref = "' + ref + '"';
    //var status =  'select status from Users where ref = "' + ref + '"';
    console.log("printing select"+ select);
    connection.query(select, function(err,result) {
      if(err) {
        console.log('Error while checking the records in database' + err);
        //res.send('Error while checking the records in database' + err);
        //s = 'no';
      }
      else {
        console.log("successsss");
        console.log(result[0].tot);
        s = 'yes';
        if(result[0].tot == 1){
          res.send('valid');
        }else{
          res.send('invalid');
        }
       //if the result is empty. Insert the record....
      }
    });
    connection.release();
    });

  });

//check if the email already exits...
router.post('/get_reg', function(req, res){
  var currentTime = new Date();
  var currentOffset = currentTime.getTimezoneOffset();
  var ISTOffset = 330;   // IST offset UTC +5:30
  var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);
  if(req.body.phone == ""){
    req.body.phone = 1234567890;
  }
  var s = '';
  mysql_pool.getConnection(function(err, connection) {
    if (err) {

      console.log(' Error getting mysql_pool connection: ' + err);
      throw err;
    }
    //check if the email is already registered...
    var user_email = req.body.email;
    var select = 'select count(email) as tot from Users where email = "' + req.body.email + '"';

    connection.query(select, function(err,result) {
      if(err) {
        //res.send('Error while checking the records in database' + err);
        //s = 'no';
      }
      else {
        s = 'yes';
       //if the result is empty. Insert the record....
      }
    });
    connection.release();
  });



});
//insert the new user details...
router.post('/set_reg', function(req, res){
  console.log(req.body);
  var currentTime = new Date();
  var currentOffset = currentTime.getTimezoneOffset();
  var ISTOffset = 330;   // IST offset UTC +5:30
  var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);
  if(req.body.phone == ""){
    req.body.phone = 1234567890;
  }
  var s = '';
  mysql_pool.getConnection(function(err, connection) {
    if (err) {
      //
      console.log(' Error getting mysql_pool connection: ' + err);
      throw err;
    }
    var ref = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'

    var select = 'select count(*) as tot from Users where email = "' + req.body.email + '"';
    //var status =  'select status from Users where ref = "' + ref + '"';
    console.log("printing select"+ select);
    connection.query(select, function(err,result) {
      if(err) {
        console.log('Error while checking the records in database' + err);
        //res.send('Error while checking the records in database' + err);
        //s = 'no';
      }
      else {
        console.log("successsss");
        console.log(result[0].tot);
        if(result[0].tot >= 1){
          res.send('existingemail');
        }else{
          var values = [ref,req.body.name,req.body.email.toLowerCase(),'',req.body.phone,req.body.gender,'pending',ISTTime,req.body.address,req.body.country,req.body.state,req.body.city,req.body.pincode,req.body.whatsapp,req.body.mail,req.body.strongHuman];
          console.log("user record to be inserted" + values);
          //var posts = {ref: ref,fname: req.body.fname,lname: req.body.lname,email: req.body.email,password: req.body.pwd,phone: '1234567980',status: 'pending',create_date: ISTTime}

          connection.query('INSERT INTO Users (ref, name, email, password, phone, gender, status, create_date, address, country, state, city, pincode,whatsapp,mail,stronghuman) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', values, function(err,result) {
          //connection.query('INSERT INTO users SET ?', posts, function(err,result) {
           console.log("entering db query");
            if(err) {
              console.log(err)
              res.send('dbissue');
            }
            else {
              //encrypt the ref string for vaidation purpose..
              var buffer = new Buffer(ref);
              var toBase64 = buffer.toString('base64');
             console.log("insertion into user table successful");
             var emailbody = fs.readFileSync('./static/html/signup.html').toString();
             emailbody = emailbody.replace("FNAME", req.body.name);
             var activationlink = constants.homepage + "/Auth/" + toBase64;
             emailbody = emailbody.replace("UPDATE_PROFILE", activationlink);
             emailbody = emailbody.replace("UNSUB1", constants.homepage);

          console.log(toBase64);
              var transporter = nodemailer.createTransport({
             host: '192.185.129.72',
             port: 25,
             secure: false,
             auth: {
               user: 'autosend@shn.world',
               pass: 'Restart@123'
             },
             tls:{
               rejectUnauthorized: false
             }
           });

                     var mailOptions = {
             from: 'autosend@shn.world',
             to: req.body.email,
             subject: 'SHN Activation link',
             text: 'That was easy!',
             html: emailbody
           };

           transporter.sendMail(mailOptions, function(error, info){
             if (error) {
               console.log(error);
             } else {
               console.log('Email sent: ' + info.response);
             }
           });

                     // // send mail with defined transport object
                     // transporter.sendMail(mailOptions, (error, info) => {
                     //     if (error) {
                     //         return console.log(error);
                     //     }
                     //     console.log('Message sent: %s', info.messageId);

                     //     // Preview only available when sending through an Ethereal account

                     //     console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                     //     res.json({code:1, messageId: info.messageId, messageURL: nodemailer.getTestMessageUrl(info)});

                     // });


             res.send('Successful insertion into database');
            }
          });        }
       //if the result is empty. Insert the record....
      }
    });
    connection.release();
  });
});

//save password..
router.post('/set_reg_pwd', function(req, res){
  var currentTime = new Date();
  var currentOffset = currentTime.getTimezoneOffset();
  var ISTOffset = 330;   // IST offset UTC +5:30
  var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);

  hashPassword(req.body.password, function(hpwd){
    mysql_pool.getConnection(function(err, connection) {
      if (err) {

        console.log(' Error getting mysql_pool connection: ' + err);
        throw err;
      }
      var buffer = req.body.ref;
      var ref = Buffer.from(buffer, 'base64');
      buffer = new Buffer(ref);
      ref = buffer.toString('ascii');
      //console.log("Hashed password" + hashPassword(req.body.password))
      var values = [hpwd, ref];
      console.log(hpwd)
     connection.query("UPDATE Users SET password = ?, status='active' WHERE ref = ?", values, function(err,result) {

       if(err) {
         console.log(err)
         res.send('Error while inserting into database' + err );
       }
       else {
         if(result.affectedRows == 1){
            connection.query("UPDATE Users SET ref = NULL  WHERE ref = ?", ref, function(err,result) {
              if(err) {
                console.log(err)
                res.send('Error while inserting into database' + err );
              }
              else {
                res.send('valid');
              }
            });
         }
         else{
            res.send('ref id invalid');
         }
       }
     });

      connection.release();
    });
  });
});

//check if the valid user
router.post('/reg', function(req, res){
  var currentTime = new Date();
  var currentOffset = currentTime.getTimezoneOffset();
  var ISTOffset = 330;   // IST offset UTC +5:30
  var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);

    mysql_pool.getConnection(function(err, connection) {
      if (err) {

        console.log(' Error getting mysql_pool connection: ' + err);
        throw err;
      }
      var user_email = req.body.email;
      var select = 'select name, email, password, showcareers from Users where email = "' + req.body.email + '"';
      console.log("select query  " + select);
      connection.query(select, function(err,result) {
        if(err) {
          console.log('Error while checking the records in database' + err);
          res.send({
            "code":400,
            "failed":"error ocurred"
          })
        }
        else {
          //console.log(result[0] + "       " + hpwd);
          if(result.length >0){
            //var stored_hash = '$2a$10$vxliJ./aXotlnxS9HaJoXeeASt48.ddU7sHNOpXC/cLhgzJGdASCe'
            //bcrypt.compare(guess, stored_hash, function(err, res) {});
            bcrypt.compare(req.body.password, result[0].password, function(err, passwordMatch) {
              if(err){
                res.status(401).json({
                      success: false,
                      token: null,
                      err: 'Email does not exist'
                });
              }
              else{

                if(passwordMatch){
                  //If all credentials are correct do this
                  // Sigining the token
                  let token = jwt.sign({ showcareers: result[0].showcareers, email: result[0].email }, constants.jwtsecret, { expiresIn: 129600 });
                  res.json({
                      success: true,
                      err: null,
                      token
                  });

                }
                else{
                  // res.send('Email and Password does not match');
                  res.status(401).json({
                      success: false,
                      token: null,
                      err: 'Username or Password is incorrect'
                  });
                }
              }
            });
          }
          else{
            // res.send('Email does not exist');
            res.status(401).json({
                  success: false,
                  token: null,
                  err: 'Email does not exist'
            });
          }
        }
      });
      connection.release();
    });
});

//Forgot Password method Functionality
router.post('/forgot_password', function(req, res){
  console.log(req.body);

  mysql_pool.getConnection(function(err, connection) {
    if (err) {
      console.log(' Error getting mysql_pool connection: ' + err);
      throw err;
    }

    var ref = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var select = 'select count(*) as tot from Users where email = "' + req.body.email + '"';
    console.log("printing select"+ select);
    connection.query(select, function(err,result) {
      if(err) {
        console.log('Error while checking the records in database' + err);
      }
      else {
        console.log("successsss");
        console.log(result[0].tot);

        if(result[0].tot >= 1){
          var values = [ref,req.body.email];
          console.log("user record to be inserted" + values);
          //var posts = {ref: ref,fname: req.body.fname,lname: req.body.lname,email: req.body.email,password: req.body.pwd,phone: '1234567980',status: 'pending',create_date: ISTTime}
          var fullName;
          connection.query("select name as fullName from Users WHERE email = ?", req.body.email, function(err,result) {
            if(err) {
              console.log('Error while checking the records in database' + err);
            }
            else {
              fullName = result[0].fullName;
              console.log(fullName);
            }
          });
          connection.query("UPDATE Users SET ref = ?  WHERE email = ?", values, function(err,result) {
          //connection.query('INSERT INTO users SET ?', posts, function(err,result) {
           console.log("entering db query");
            if(err) {
              console.log(err)
              res.send('Error while inserting into database' + err + ' === > ' +req.body.name);
            }
            else {
              //encrypt the ref string for vaidation purpose..
                      var buffer = new Buffer(ref);
                      var toBase64 = buffer.toString('base64');
                      console.log("insertion into user table successful");
                      var emailbody = fs.readFileSync('./static/html/forgot_password.html').toString();
                      emailbody = emailbody.replace("FNAME", fullName);
                      var activationlink = constants.homepage + "/Auth/" + toBase64;
                      emailbody = emailbody.replace("UPDATE_PROFILE", activationlink);
                      emailbody = emailbody.replace("UNSUB1", constants.homepage);


                      console.log(toBase64);
                      var transporter = nodemailer.createTransport({
                           host: 'mail.shn.world',
                           port: 25,
                           secure: false,
                           auth: {
                             user: 'autosend@shn.world',
                             pass: 'Restart@123'
                           },
                           tls:{
                             rejectUnauthorized: false
                           }
                         });

                         var mailOptions = {
                             from: 'autosend@shn.world',
                             to: req.body.email,
                             subject: 'Reset Password',
                             text: 'That was easy!',
                             html: emailbody
                         };

                      transporter.sendMail(mailOptions, function(error, info){
                         if (error) {
                                console.log(error);
                         } else {
                                console.log('Email sent: ' + info.response);
                         }
                         });
                     res.send('existing email');
                }
           });
        }
        else{
          res.send('invalid');

        }
       //if the result is empty. Insert the record....
      }
    });
    connection.release();
  });
});



function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

//Pasword Hashing Functionality
function hashPassword(pwd, callback){
 bcrypt.genSalt(4, function(err, salt){
    if(err){
      return err;
    }
    console.log('salt : ' + salt)
    bcrypt.hash(pwd, salt, function(err, hpwd){
      if(err){
        return err;
      }
      console.log('pwd :' + pwd)
      console.log('hash : '+ hpwd)
      callback(hpwd);
    })
  })
}


//Update showcareers page
router.post('/showCareers', function(req, res){
//  console.log(req.body);
mysql_pool.getConnection(function(err, connection) {
      if (err) {
        console.log(' Error getting mysql_pool connection: ' + err);
        throw err;
      }
      var values = [req.body.showcareers,req.body.email];
    //  console.log(values);
     connection.query("UPDATE Users SET showcareers = ? WHERE email = ?", values, function(err,result) {

       if(err) {
         console.log(err)
         res.send('Error while inserting into database' + err );
       }
       else {
         let token = jwt.sign({ showcareers: true, email: req.body.email }, constants.jwtsecret, { expiresIn: 129600 });
         console.log(token + " " + req.body.email);
         res.json({
             success: true,
             err: null,
             token
           })
         }
       });
       connection.release();
     });
});


// var transporter = nodemailer.createTransport({
//   service: 'gmail',
//   auth: {
//     user: 'xxxxx',
//     pass: 'xxx'
//   }
// });

// var mailOptions = {
//   from: 'satya@shn.world',
//   to: 'srinumadduri@shn.world',
//   subject: 'Sending Email using Node.js',
//   text: 'That was easy!'
// };

// transporter.sendMail(mailOptions, function(error, info){
//   if (error) {
//     console.log(error);
//   } else {
//     console.log('Email sent: ' + info.response);
//   }
// });

//export this router to use in our index.js
module.exports = router;
